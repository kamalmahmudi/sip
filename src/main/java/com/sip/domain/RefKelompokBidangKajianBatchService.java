package com.sip.domain;
import org.gvnix.addon.jpa.annotations.batch.GvNIXJpaBatch;
import org.springframework.stereotype.Service;

@Service
@GvNIXJpaBatch(entity = RefKelompokBidangKajian.class)
public class RefKelompokBidangKajianBatchService {
}
