package com.sip.web;
import com.sip.domain.RefMetodologi;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import com.sip.domain.RefMetodologiBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/refmetodologis")
@Controller
@RooWebScaffold(path = "refmetodologis", formBackingObject = RefMetodologi.class)
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = RefMetodologiBatchService.class)
@GvNIXDatatables(ajax = true)
public class RefMetodologiController {
}
