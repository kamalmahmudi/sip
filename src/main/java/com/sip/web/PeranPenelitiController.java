package com.sip.web;
import com.sip.domain.PeranPeneliti;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import com.sip.domain.PeranPenelitiBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/peranpenelitis")
@Controller
@RooWebScaffold(path = "peranpenelitis", formBackingObject = PeranPeneliti.class)
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = PeranPenelitiBatchService.class)
@GvNIXDatatables(ajax = true)
public class PeranPenelitiController {
}
