// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.sip.domain;

import com.sip.domain.RefPenyandangDana;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect RefPenyandangDana_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager RefPenyandangDana.entityManager;
    
    public static final List<String> RefPenyandangDana.fieldNames4OrderClauseFilter = java.util.Arrays.asList("");
    
    public static final EntityManager RefPenyandangDana.entityManager() {
        EntityManager em = new RefPenyandangDana().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long RefPenyandangDana.countRefPenyandangDanas() {
        return entityManager().createQuery("SELECT COUNT(o) FROM RefPenyandangDana o", Long.class).getSingleResult();
    }
    
    public static List<RefPenyandangDana> RefPenyandangDana.findAllRefPenyandangDanas() {
        return entityManager().createQuery("SELECT o FROM RefPenyandangDana o", RefPenyandangDana.class).getResultList();
    }
    
    public static List<RefPenyandangDana> RefPenyandangDana.findAllRefPenyandangDanas(String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM RefPenyandangDana o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, RefPenyandangDana.class).getResultList();
    }
    
    public static RefPenyandangDana RefPenyandangDana.findRefPenyandangDana(Integer idPenyandangDana) {
        if (idPenyandangDana == null) return null;
        return entityManager().find(RefPenyandangDana.class, idPenyandangDana);
    }
    
    public static List<RefPenyandangDana> RefPenyandangDana.findRefPenyandangDanaEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM RefPenyandangDana o", RefPenyandangDana.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<RefPenyandangDana> RefPenyandangDana.findRefPenyandangDanaEntries(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM RefPenyandangDana o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, RefPenyandangDana.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void RefPenyandangDana.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void RefPenyandangDana.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            RefPenyandangDana attached = RefPenyandangDana.findRefPenyandangDana(this.idPenyandangDana);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void RefPenyandangDana.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void RefPenyandangDana.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public RefPenyandangDana RefPenyandangDana.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        RefPenyandangDana merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
