// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.sip.domain;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.PathBuilder;
import com.sip.domain.RefPersyaratan;
import com.sip.domain.RefPersyaratanBatchService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.transaction.annotation.Transactional;

privileged aspect RefPersyaratanBatchService_Roo_GvNIXJpaBatch {
    
    public Class RefPersyaratanBatchService.getEntity() {
        return RefPersyaratan.class;
    }
    
    public EntityManager RefPersyaratanBatchService.entityManager() {
        return RefPersyaratan.entityManager();
    }
    
    @Transactional
    public int RefPersyaratanBatchService.deleteAll() {
        return entityManager().createQuery("DELETE FROM RefPersyaratan").executeUpdate();
    }
    
    @Transactional
    public int RefPersyaratanBatchService.deleteIn(List<Integer> ids) {
        Query query = entityManager().createQuery("DELETE FROM RefPersyaratan as r WHERE r.idSyarat IN (:idList)");
        query.setParameter("idList", ids);
        return query.executeUpdate();
    }
    
    @Transactional
    public int RefPersyaratanBatchService.deleteNotIn(List<Integer> ids) {
        Query query = entityManager().createQuery("DELETE FROM RefPersyaratan as r WHERE r.idSyarat NOT IN (:idList)");
        query.setParameter("idList", ids);
        return query.executeUpdate();
    }
    
    @Transactional
    public void RefPersyaratanBatchService.create(List<RefPersyaratan> refPersyaratans) {
        for( RefPersyaratan refpersyaratan : refPersyaratans) {
            refpersyaratan.persist();
        }
    }
    
    @Transactional
    public List<RefPersyaratan> RefPersyaratanBatchService.update(List<RefPersyaratan> refpersyaratans) {
        List<RefPersyaratan> merged = new ArrayList<RefPersyaratan>();
        for( RefPersyaratan refpersyaratan : refpersyaratans) {
            merged.add( refpersyaratan.merge() );
        }
        return merged;
    }
    
    public List<RefPersyaratan> RefPersyaratanBatchService.findByValues(Map<String, Object> propertyValues) {
        
        // if there is a filter
        if (propertyValues != null && !propertyValues.isEmpty()) {
            // Prepare a predicate
            BooleanBuilder baseFilterPredicate = new BooleanBuilder();
            
            // Base filter. Using BooleanBuilder, a cascading builder for
            // Predicate expressions
            PathBuilder<RefPersyaratan> entity = new PathBuilder<RefPersyaratan>(RefPersyaratan.class, "entity");
            
            // Build base filter
            for (String key : propertyValues.keySet()) {
                baseFilterPredicate.and(entity.get(key).eq(propertyValues.get(key)));
            }
            
            // Create a query with filter
            JPAQuery query = new JPAQuery(RefPersyaratan.entityManager());
            query = query.from(entity);
            
            // execute query
            return query.where(baseFilterPredicate).list(entity);
        }
        
        // no filter: return all elements
        return RefPersyaratan.findAllRefPersyaratans();
    }
    
    @Transactional
    public long RefPersyaratanBatchService.deleteByValues(Map<String, Object> propertyValues) {
        
        // if there no is a filter
        if (propertyValues == null || propertyValues.isEmpty()) {
            throw new IllegalArgumentException("Missing property values");
        }
        // Prepare a predicate
        BooleanBuilder baseFilterPredicate = new BooleanBuilder();
        
        // Base filter. Using BooleanBuilder, a cascading builder for
        // Predicate expressions
        PathBuilder<RefPersyaratan> entity = new PathBuilder<RefPersyaratan>(RefPersyaratan.class, "entity");
        
        // Build base filter
        for (String key : propertyValues.keySet()) {
            baseFilterPredicate.and(entity.get(key).eq(propertyValues.get(key)));
        }
        
        // Create a query with filter
        JPADeleteClause delete = new JPADeleteClause(RefPersyaratan.entityManager(),entity);
        
        // execute delete
        return delete.where(baseFilterPredicate).execute();
    }
    
    @Transactional
    public void RefPersyaratanBatchService.delete(List<RefPersyaratan> refpersyaratans) {
        for( RefPersyaratan refpersyaratan : refpersyaratans) {
            refpersyaratan.remove();
        }
    }
    
}
