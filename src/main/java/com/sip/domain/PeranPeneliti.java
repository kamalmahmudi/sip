package com.sip.domain;
import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooJpaActiveRecord(identifierType = PeranPenelitiPK.class, versionField = "", table = "peran_peneliti")
@RooDbManaged(automaticallyDelete = true)
@RooToString(excludeFields = { "idPenelitian", "idPeneliti" })
public class PeranPeneliti {
}
