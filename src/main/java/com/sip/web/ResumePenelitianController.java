package com.sip.web;
import com.sip.domain.ResumePenelitian;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import com.sip.domain.ResumePenelitianBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/resumepenelitians")
@Controller
@RooWebScaffold(path = "resumepenelitians", formBackingObject = ResumePenelitian.class)
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = ResumePenelitianBatchService.class)
@GvNIXDatatables(ajax = true)
public class ResumePenelitianController {
}
