SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT=0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


DROP TABLE IF EXISTS `luaran_penelitian`;
CREATE TABLE IF NOT EXISTS `luaran_penelitian` (
  `ID_LUARAN` int(11) NOT NULL,
  `ID_PENELITIAN` int(11) NOT NULL,
  `DESKRIPSI` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID_LUARAN`,`ID_PENELITIAN`),
  KEY `FK_LUARAN_PENELITIAN2` (`ID_PENELITIAN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `master_institusi_peneliti`;
CREATE TABLE IF NOT EXISTS `master_institusi_peneliti` (
  `ID_INSTITUSI` int(11) NOT NULL AUTO_INCREMENT,
  `INSTITUSI` varchar(100) NOT NULL,
  `DEPARTMENT` varchar(150) DEFAULT NULL,
  `PRODI` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`ID_INSTITUSI`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=141 ;

INSERT INTO `master_institusi_peneliti` (`ID_INSTITUSI`, `INSTITUSI`, `DEPARTMENT`, `PRODI`) VALUES
(1, 'UNIVERSITAS MALIKUSSALEH', '', ''),
(2, 'UNIVERSITAS SYIAH KUALA', '', ''),
(3, 'UNIVERSITAS NEGERI MEDAN', '', ''),
(4, 'UNIVERSITAS SUMATERA UTARA', '', ''),
(5, 'UNIVERSITAS ANDALAS', '', ''),
(6, 'UNIVERSITAS NEGERI PADANG', '', ''),
(7, 'UNIVERSITAS ISLAM NEGERI SULTAN SYARIF KASIM', '', ''),
(8, 'UNIVERSITAS RIAU', '', ''),
(9, 'UNIVERSITAS MARITIM RAJA ALI HAJI', '', ''),
(10, 'UNIVERSITAS JAMBI', '', ''),
(11, 'UNIVERSITAS BENGKULU', '', ''),
(12, 'UNIVERSITAS ISLAM NEGERI RADEN FATAH', '', ''),
(13, 'UNIVERSITAS SRIWIJAYA', '', ''),
(14, 'UNIVERSITAS BANGKA BELITUNG', '', ''),
(15, 'UNIVERSITAS LAMPUNG', '', ''),
(16, 'UNIVERSITAS INDONESIA', '', ''),
(17, 'UNIVERSITAS ISLAM NEGERI JAKARTA', '', ''),
(18, 'UNIVERSITAS NEGERI JAKARTA', '', ''),
(19, 'INSTITUT PERTANIAN BOGOR', '', ''),
(20, 'INSTITUT TEKNOLOGI BANDUNG', '  SEKOLAH TEK. ELEKTRO & INFORMATIKA (STEI)', 'Teknik Informatika'),
(21, 'UNIVERSITAS ISLAM NEGERI SUNAN GUNUNG DJATI', '', ''),
(22, 'UNIVERSITAS PADJADJARAN', '', ''),
(23, 'UNIVERSITAS PENDIDIKAN INDONESIA', '', ''),
(24, 'UNIVERSITAS DIPONEGORO', '', ''),
(25, 'UNIVERSITAS ISLAM NEGERI WALISONGO', '', ''),
(26, 'UNIVERSITAS JENDERAL SOEDIRMAN', '', ''),
(27, 'UNIVERSITAS NEGERI SEMARANG', '', ''),
(28, 'UNIVERSITAS SEBELAS MARET', '', ''),
(29, 'UNIVERSITAS GADJAH MADA', '', ''),
(30, 'UNIVERSITAS ISLAM NEGERI SUNAN KALIJAGA', '', ''),
(31, 'UNIVERSITAS NEGERI YOGYAKARTA', '', ''),
(32, 'INSTITUT TEKNOLOGI SEPULUH NOPEMBER', '', ''),
(33, 'UNIVERSITAS AIRLANGGA', '', ''),
(34, 'UNIVERSITAS BRAWIJAYA', '', ''),
(35, 'UNIVERSITAS ISLAM NEGERI MALANG', '', ''),
(36, 'UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA', '', ''),
(37, 'UNIVERSITAS JEMBER', '', ''),
(38, 'UNIVERSITAS NEGERI MALANG', '', ''),
(39, 'UNIVERSITAS NEGERI SURABAYA', '', ''),
(40, 'UNIVERSITAS TRUNOJOYO', '', ''),
(41, 'UNIVERSITAS SULTAN AGENG TIRTAYASA', '', ''),
(42, 'UNIVERSITAS PENDIDIKAN GANESHA', '', ''),
(43, 'UNIVERSITAS UDAYANA', '', ''),
(44, 'UNIVERSITAS MATARAM', '', ''),
(45, 'UNIVERSITAS NUSA CENDANA', '', ''),
(46, 'UNIVERSITAS TANJUNGPURA', '', ''),
(47, 'UNIVERSITAS PALANGKARAYA', '', ''),
(48, 'UNIVERSITAS LAMBUNG MANGKURAT', '', ''),
(49, 'UNIVERSITAS MULAWARMAN', '', ''),
(50, 'UNIVERSITAS BORNEO TARAKAN', '', ''),
(51, 'UNIVERSITAS NEGERI MANADO', '', ''),
(52, 'UNIVERSITAS SAM RATULANGI', '', ''),
(53, 'UNIVERSITAS TADULAKO', '', ''),
(54, 'UNIVERSITAS HASANUDDIN', '', ''),
(55, 'UNIVERSITAS ISLAM NEGERI ALAUDDIN', '', ''),
(56, 'UNIVERSITAS NEGERI MAKASSAR', '', ''),
(57, 'UNIVERSITAS HALUOLEO', '', ''),
(58, 'UNIVERSITAS NEGERI GORONTALO', '', ''),
(59, 'UNIVERSITAS PATTIMURA', '', ''),
(60, 'UNIVERSITAS KHAIRUN', '', ''),
(61, 'UNIVERSITAS CENDERAWASIH', '', ''),
(62, 'UNIVERSITAS MUSAMUS MERAUKE', '', ''),
(63, 'UNIVERSITAS PAPUA', '', ''),
(64, 'Politeknik Negeri Lhokseumawe (PNL)', '', ''),
(65, 'Politeknik Negeri Medan (POLMED)', '', ''),
(66, 'Politeknik Negeri Padang (PNP)', '', ''),
(67, 'Politeknik Pertanian Negeri Payakumbuh (POLITANI)', '', ''),
(68, 'Politeknik Negeri Bengkalis (POLBENG)', '', ''),
(69, 'Politeknik Negeri Batam (POLIBATAM)', '', ''),
(70, 'Politeknik Negeri Sriwijaya (POLSRI), Palembang', '', ''),
(71, 'Politeknik Manufaktur Negeri Bangka Belitung', '', ''),
(72, 'Politeknik Negeri Lampung (POLINELA)', '', ''),
(73, 'Politeknik Negeri Jakarta (PNJ)', '', ''),
(74, 'Politeknik Negeri Media Kreatif (POLIMEDIA)', '', ''),
(75, 'Politeknik Manufaktur Negeri Bandung (POLMAN)', '', ''),
(76, 'Politeknik Negeri Indramayu (POLINDRA)', '', ''),
(77, 'Politeknik Negeri Semarang (POLINES)', '', ''),
(78, 'Politeknik Maritim Negeri Indonesia, Semarang', '', ''),
(79, 'Politeknik Negeri Cilacap,', '', ''),
(80, 'Politeknik Elektronika Negeri Surabaya (PENS)', '', ''),
(81, 'Politeknik Perkapalan Negeri Surabaya (PPNS)', '', ''),
(82, 'Politeknik Negeri Malang (POLINEMA)', '', ''),
(83, 'Politeknik Negeri Jember (POLIJE)', '', ''),
(84, 'Politeknik Negeri Madiun (PNM)', '', ''),
(85, 'Politeknik Negeri Madura (POLTERA)', '', ''),
(86, 'Politeknik Negeri Banyuwangi (POLIWANGI)', '', ''),
(87, 'Politeknik Negeri Pontianak (POLNEP)', '', ''),
(88, 'Politeknik Negeri Sambas (POLTESA)', '', ''),
(89, 'Politeknik Negeri Ketapang (POLITAP)', '', ''),
(90, 'Politeknik Negeri Banjarmasin (POLIBAN)', '', ''),
(91, 'Politeknik Negeri Tanah Laut (POLITALA)', '', ''),
(92, 'Politeknik Negeri Balikpapan (POLTEKBA)', '', ''),
(93, 'Politeknik Negeri Samarinda (POLNES)', '', ''),
(94, 'Politeknik Pertanian Negeri Samarinda (POLTANESA)', '', ''),
(95, 'Politeknik Negeri Bali (PNB)', '', ''),
(96, 'Politeknik Negeri Kupang (PNK)', '', ''),
(97, 'Politeknik Pertanian Negeri Kupang', '', ''),
(98, 'Politeknik Negeri Ujung Pandang', '', ''),
(99, 'Politeknik Pertanian Negeri Pangkajene dan Kepulauan', '', ''),
(100, 'Politeknik Negeri Manado (POLIMDO)', '', ''),
(101, 'Politeknik Negeri Nusa Utara (POLINUSA)', '', ''),
(102, 'Politeknik Negeri Ambon (POLNAM)', '', ''),
(103, 'Politeknik Perikanan Negeri Tual (POLIKANI)', '', ''),
(104, 'Politeknik Negeri FakFak', '', ''),
(105, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Sipil', 'Program Studi D3 Teknik Konstruksi Gedung'),
(106, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Sipil', 'Program Studi D3 Teknik Konstruksi Sipil'),
(107, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Sipil', 'Program Studi D4 Teknik Perancangan Jalan dan Jembatan'),
(108, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Sipil', 'Program Studi D4 Teknik Perawatan dan Perbaikan Gedung'),
(109, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Mesin', 'Program Studi D3 Teknik Mesin'),
(110, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Mesin', 'Program Studi D3 Teknik Aeronautika'),
(111, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Mesin', 'Program Studi D4 Teknik Perancangan dan Konstruksi Mesin'),
(112, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Mesin', 'Program Studi D4 Rekayasa Teknologi Manufaktur'),
(113, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Konversi Energi', 'Program Studi D3 Teknik Konversi Energi'),
(114, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Konversi Energi', 'Program Studi D4 Teknologi Pembangkit Tenaga Listrik'),
(115, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Konversi Energi', 'Program Studi D4 Teknik Konservasi Energi'),
(116, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Refrigerasi dan Tata Udara', 'Program Studi D3 Teknik Refrigerasi dan Tata Udara'),
(117, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Refrigerasi dan Tata Udara', 'Program Studi D4 Teknik Pendingin dan Tata Udara'),
(118, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Elektro', 'Program Studi D3 Teknik Elektronika'),
(119, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Elektro', 'Program Studi D3 Teknik Listrik'),
(120, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Elektro', 'Program Studi D3 Teknik Telekomunikasi'),
(121, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Elektro', 'Program Studi D4 Teknik Telekomunikasi Nirkabel'),
(122, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Elektro', 'Program Studi D4 Teknik Elektronika'),
(123, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Elektro', 'Program Studi D4 Teknik Otomasi Industri'),
(124, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Kimia', 'Program Studi D3 Teknik Kimia'),
(125, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Kimia', 'Program Studi D3 Analis Kimia'),
(126, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Kimia', 'Program Studi D4 Teknik Kimia Produksi Bersih'),
(127, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Komputer dan Informatika', 'Program Studi D3 Teknik Informatika'),
(128, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Teknik Komputer dan Informatika', 'Program Studi D4 Teknik Informatika'),
(129, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Akuntansi', 'Program Studi D3 Akuntansi'),
(130, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Akuntansi', 'Program Studi D3 Keuangan Perbankan'),
(131, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Akuntansi', 'Program Studi D4 Akuntansi Manajemen Pemerintahan'),
(132, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Akuntansi', 'Program Studi D4 Keuangan Syariah'),
(133, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Akuntansi', 'Program Studi D4 Akuntansi'),
(134, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Admnistrasi Niaga', 'Program Studi D3 Administrasi Bisnis'),
(135, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Admnistrasi Niaga', 'Program Studi D3 Manajemen Pemasaran'),
(136, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Admnistrasi Niaga', 'Program Studi D3 Usaha Perjalanan Wisata'),
(137, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Admnistrasi Niaga', 'Program Studi D4 Administrasi Bisnis'),
(138, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Admnistrasi Niaga', 'Program Studi D4 Manajemen Aset'),
(139, 'Politeknik Negeri Bandung (POLBAN)', 'Jurusan Bahasa Inggris', 'Program Studi D3 Bahasa Inggris'),
(140, 'Politeknik Negeri Bandung (POLBAN)', 'UP MKU', '');

DROP TABLE IF EXISTS `master_peneliti`;
CREATE TABLE IF NOT EXISTS `master_peneliti` (
  `ID_PENELITI` int(11) NOT NULL AUTO_INCREMENT,
  `ID_INSTITUSI` int(11) NOT NULL,
  `NAMA_PENELITI` varchar(150) NOT NULL,
  `NIDN` varchar(25) DEFAULT NULL,
  `JENIS_KELAMIN` tinyint(1) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID_PENELITI`),
  KEY `FK_BERASAL_DARI` (`ID_INSTITUSI`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

INSERT INTO `master_peneliti` (`ID_PENELITI`, `ID_INSTITUSI`, `NAMA_PENELITI`, `NIDN`, `JENIS_KELAMIN`, `EMAIL_ADDRESS`) VALUES
(1, 116, 'Drs. Pratikto, MT', '', 1, ''),
(2, 113, 'Tina Mulya Gantina, MT', '', 0, ''),
(3, 106, 'Ujang Ruslan, ST.', '', 1, ''),
(4, 117, 'C. Bambang Dwi Kuncoro, MT.', '', 1, ''),
(5, 133, 'Dadang Hermawan, SE', '', 1, ''),
(6, 119, 'Yusuf Sofyan, ST.MT', '', 1, ''),
(7, 122, 'Yana Sudarsa, BSEE.,MT', '', 1, ''),
(8, 120, 'Drs. Hasan Surya, ST., MT', '', 1, ''),
(9, 107, 'Drs. Moeljono, SP1', '', 1, ''),
(10, 117, 'Ir. Sumeru, MT', '', 1, ''),
(11, 126, 'Ir. Unung Leoanggraini, MT.', '', 0, ''),
(12, 129, 'Hastuti, SE.,Ak.', '', 0, ''),
(13, 117, 'Drs. Pratikto, MT', '', 1, ''),
(14, 112, 'Budi Hartono, ST.,MT', '', 1, ''),
(15, 111, 'Mochamad Luthfi, Dipl.Ing', '', 1, ''),
(16, 112, 'Maria F. Soetanto, Dipl.Ing.,MT', '', 0, ''),
(17, 133, 'Drs. Rendra Trisyanto S.', '', 1, ''),
(18, 108, 'Hendry, Dipl.Ing.HTL.,MT', '', 1, ''),
(19, 121, 'Vitrasia, DUT.,ST', '', 1, ''),
(20, 122, 'DR. Eril Mozef, MS.,DEA.', '', 1, ''),
(21, 122, 'Ferry Satria, BSEE.,MT', '', 1, ''),
(22, 137, 'Dwi Suhartanto, Drs.,MCM', '', 1, ''),
(23, 108, 'Iskandar, ST.MT', '', 1, ''),
(24, 112, 'Maria F. Soetanto,  Dr.,Dipl.Ing.MT', '', 0, ''),
(25, 109, 'Waluyo Musiono Bintoro, SST', '', 1, ''),
(26, 115, 'Ir. Conny K. Wachjoe', '', 1, ''),
(27, 110, 'Dr. Carolus Bintoro,MT.', '', 1, ''),
(28, 110, 'Duddy Yanpurnadi, ST.,MT.', '', 1, ''),
(29, 110, 'Drs. Abdul Karim, M.Eng. ', '', 1, ''),
(30, 110, 'Vicky Wuwung, ST.,MT', '', 1, ''),
(31, 126, 'Drs. Haryadi M.Sc., Ph.D ', '', 1, ''),
(32, 122, 'Dr. YB. Gunawan, ST., MT', '', 1, ''),
(33, 115, 'Ir. S.P. Mursid, M.Sc', '', 1, ''),
(34, 126, 'Ir. Didik Haryogi, M.Eng.', '', 1, ''),
(35, 126, 'Eko Andrijanto, LRSC. ', '', 1, ''),
(36, 124, 'Shoerya Shoelarta, Ph.D.', '', 1, ''),
(37, 124, 'Dra. Riniati, M.Si.  ', '', 0, ''),
(38, 124, 'Ir. Heriyanto, MT.', '', 1, ''),
(39, 124, 'Ir. Herawati Budiastuti,M.Eng.Sc,,PhD. ', '', 0, ''),
(40, 125, 'Dra. Sri Widarti, M.Si.,M.Sc', '', 0, ''),
(42, 108, 'Ir. Mei Sutrisno M.Sc, Ph.D. ', '', 1, ''),
(43, 108, 'Enung, ST., M.Eng.  ', '', 0, ''),
(44, 108, 'Fisca Igustiany, SST., MT.', '', 0, ''),
(45, 128, 'Ani Rahmani S.Si., M.T. ', '', 0, ''),
(46, 128, ' Joe Lian Min, M.Eng. ', '', 1, ''),
(47, 127, 'Santi Sundari, S.Si.,M.T.', '', 0, ''),
(48, 140, 'Drs. Sardjito, MSc.', '', 1, ''),
(49, 140, 'Nani Yuningsih, S.Si.,M.Si.  ', '', 0, ''),
(50, 140, 'Dra. Kunlestiowati H.,M.Si.  ', '', 0, ''),
(51, 117, 'Ir. Andriyanto Setyawan, M.T. ', '', 1, ''),
(52, 117, 'Apip Badarudin, ST.,MT. ', '', 1, ''),
(53, 116, 'AP. Edi Sukamto, ST.,MT.', '', 1, ''),
(54, 128, 'Ade Chandra Nugraha S.Si., M.T', '', 1, ''),
(56, 111, 'Adri Maldi Subardjah,B.Eng(Hons).,MSC', '', 1, ''),
(57, 111, 'Ir. Ali Mahmudi, M.Eng.', '', 1, ''),
(58, 109, 'Aris Suryadi,MT. ', '', 1, ''),
(59, 109, ' Ir. M. Munir Fahmi, M.T.', '', 1, ''),
(60, 133, 'Diharpi Herli Setyowati, S.E.,M.Si. ', '', 0, ''),
(61, 133, 'Dr. Muhammad Muflih, MA.', '', 1, ''),
(62, 127, 'Suprihanto, BSEE.,M.Sc.', '', 1, ''),
(63, 128, ' Transmissia Semiawan, Ph.D.', '', 0, ''),
(64, 20, 'Dr. Inggriani Liem', '', 0, ''),
(65, 4, '', '', 0, '');

DROP TABLE IF EXISTS `metodologi_penelitian`;
CREATE TABLE IF NOT EXISTS `metodologi_penelitian` (
  `ID_PENELITIAN` int(11) NOT NULL,
  `ID_METODOLOGI` int(11) NOT NULL,
  PRIMARY KEY (`ID_PENELITIAN`,`ID_METODOLOGI`),
  KEY `FK_METODOLOGI_PENELITIAN2` (`ID_METODOLOGI`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `penelitian`;
CREATE TABLE IF NOT EXISTS `penelitian` (
  `ID_PENELITIAN` int(11) NOT NULL AUTO_INCREMENT,
  `ID_BIDANG_KAJIAN` int(11) NOT NULL,
  `ID_SKEMA` int(11) NOT NULL,
  `JUDUL` varchar(250) NOT NULL,
  `TAHUN_ANGGARAN` char(4) DEFAULT NULL,
  `DURASI` int(11) DEFAULT NULL,
  `NOMOR_KONTRAK` varchar(50) DEFAULT NULL,
  `NILAI_DANA` decimal(12,0) DEFAULT NULL,
  PRIMARY KEY (`ID_PENELITIAN`),
  KEY `FK_BIDANG_KAJIANPENELITIAN` (`ID_BIDANG_KAJIAN`),
  KEY `FK_MEMILIKI` (`ID_SKEMA`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

INSERT INTO `penelitian` (`ID_PENELITIAN`, `ID_BIDANG_KAJIAN`, `ID_SKEMA`, `JUDUL`, `TAHUN_ANGGARAN`, `DURASI`, `NOMOR_KONTRAK`, `NILAI_DANA`) VALUES
(1, 26, 11, 'Analisa Karakteristik Transmisi Bluetooth Karena Interferensi Peralatan yang Bekerja pada Frekuensi 2.4 GHz', '2005', 1, '', 3600000),
(2, 26, 11, 'Pengaruh Penggunaan Bahan Bakar Campuran Solar-Minyak Tanah Terhadap Performansi Mesin Diesel', '2005', 1, '', 3600000),
(3, 25, 11, 'Pemodelan Panel Dinding Dari Beton Ringan untuk Dinding Rumah Tinggal', '2005', 1, '', 3600000),
(4, 26, 11, 'Analisa Karakteristik Transmisi IEEE 802.11B Network pada Ruangan Bersekat dan Berdinding Beton Dengan Ketebalan Dinding 20 cm', '2005', 1, '', 3600000),
(5, 36, 11, 'Analisis Pengaruh Pemberian Kredit Bank Pasar terhadap Peningkatan Pendapatan Usaha Pedagang Kecil di Kota Cimahi', '2005', 1, '', 3600000),
(6, 27, 11, 'Rancang Bangun Low Harmonics Automatic Load Controller untuk PLTMh di PT. Cit Cimahi', '2005', 1, '', 6000000),
(7, 27, 11, 'Perancangan dan Implementasi Sistem Pengendali Otomatis Pengolahan Air Minum dengan Teknologi Reverse Osmosa Berbasis Mikrokontroller', '2005', 1, '', 6000000),
(8, 27, 11, 'Perbaikan Kualitas Daya Listrik pada Pembangkit Listrik Mikrohidro di Desa Cihanjuang', '2005', 1, '', 10000000),
(9, 25, 11, 'Pemanfaatan Air Permukaan Sungai Cibereum Untuk Kebutuhan Penggunaan Langsung dan Tidak Langsung di POLBAN', '2005', 1, '', 9830000),
(10, 26, 11, 'Produksi Irisan Jahe Kering untuk Bahan Minuman dengan Metode Freeze Drying', '2005', 1, '', 10000000),
(11, 26, 11, 'Pembuatan Screen untuk Produksi Kompos dari Kotoran Sapi untuk Mendukung Pengembangan Sentra Tanaman Hias di Desa Cihideung, Kec. Parongpong', '2005', 1, '', 10000000),
(12, 36, 11, 'Pengembangan Sistem Informasi Akuntansi: Pendekatan Dinamika Lingkungan Organisasi Perusahaan.', '2005', 1, '', 6000000),
(13, 26, 11, 'Kajian Awal Pemanfaatan SIM Card Handphone sebagai Media Penyimpangan File Data (Flash Data Memory)', '2005', 1, '', 6000000),
(14, 26, 11, 'Penggunaan Strain-Gauge dalam Proses Rancang Bangun Akselerometer untuk Mengukur Respons Getaran Frekuensi Rendah Model Sayap', '2005', 1, '', 5980000),
(15, 26, 11, 'Analisis dan Kaji Eksperimental Ciri Respon Suara dan Getaran Mekanik Sistem Lapisan Viskoelastik Bantalan Akibat Cacat untuk Mendukung Teknik Perawatan Prediktif', '2005', 1, '', 5991500),
(16, 26, 11, 'Kaji Eksperimen Efek Diameter Tip Plate pada Koefisien Aerodinamika Model Rotor Savonius', '2005', 1, '', 6000000),
(17, 36, 11, 'Analisis Model Internal Control pada  Perusahaan E-Commerce', '2005', 1, '', 4550000),
(18, 25, 11, 'Stabilisasi Tanah Lempung Padalarang sebagai Subgrade Jalan', '2005', 1, '', 6050000),
(19, 27, 7, 'Realisasi Prototipe Robot Cerdas Pendeteksi Lokasi Bayi Pada Kebakaran', '2005', 1, '', 10000000),
(20, 27, 7, 'Realisasi Prototipe Robot Pelacak Garis dan Pengenal Warna Untuk Aplikasi Penghantaran dan Pemilahan Objek-Objek Berwarna', '2005', 1, '', 10000000),
(21, 27, 7, 'Realisasi Robot Cerdas Berkaki Enam (Hexaped) Untuk Pemadam Api', '2005', 1, '', 10000000),
(22, 39, 11, 'Sikap terhadap Wanita sebagai Manajer', '2005', 1, '', 6000000),
(23, 25, 11, 'Optimalisasi Kehandalan Desain Pondasi Dengan Menggunakan Konsep Load and Resistant Factor Design (LFRD) di Wilayah Jakarta', '2005', 1, '', 6000000),
(24, 26, 11, 'Studi Komputasi Efek Rasio Tinggi Rotor Terhadap Diameter Rotor pada Vertical Low Speed Wildmill Dengan Memanfaatkan Energi Angin Sebagai Penggerak', '2005', 1, '', 6000000),
(25, 26, 11, 'Rancang Bangun Sensor Static-Dynamics Displacement yang Terkomunikasikan dengan Sistem Data Acquisisi', '2005', 1, '', 6000000),
(26, 26, 7, 'Rancang Bangun Teknologi Kompensator Aktif dan Implikasinya Terhadap Pencatatan Daya Listrik Automatis', '2005', 1, '', 150000000),
(27, 26, 7, 'Studi Pengaruh Penambahan Filler Pada Material Komposit Terhadap Pengaruh Beban Dinamik : Validasi pada Rangka Sepeda Kayuh', '2013', 5, '', 80000000),
(28, 26, 7, 'Modifikasi Membran Nafion/Silika Organik dan Aplikasinya pada Baterai Penyimpan Energi Tipe Vanadium Redox Battery ', '2014', 5, '', 110000000),
(29, 26, 7, 'Teknologi Pembuatan Sel Bahan Bakar Jenis Membran Penukar Ion Direct Ethanol Fuel Cell (DEFC) ', '2014', 5, '', 110000000),
(30, 26, 7, 'Formulasi Etanol-biodiesel dari PFAD/Limbah CPO-Antioksidan yang Diaplikasikan pada Mesin Diesel', '2013', 5, '', 50000000),
(31, 25, 7, 'Optimasi Infrastruktur Pengendali Banjir Dengan Pendekatan Value Engineering ', '2013', 5, '', 120000000),
(32, 27, 4, 'Metode Pengajaran Pemrograman Dasar Dengan Bantuan Automatic Grading System ', '2014', 2, '', 50000000),
(33, 2, 4, 'Model Biomekanik Dari Dinamika Titik Berat dan Titik Pusat Tekanan Tubuh Manusia Saat Berdiri, Berjalan, dan Berlari', '2013', 2, '', 50000000),
(34, 26, 4, 'Kajian Eksperimental Mekanisme Aliran Cincin (Annular) Cairan-Gas Dua-Fase Pada Pipa Horisontal ', '2014', 2, '', 60000000),
(35, 27, 3, 'Prototype Framework Aplikasi Pengukuran Kinerja Organisasi ', '2013', 2, '', 57500000),
(36, 26, 3, 'Rancang Bangun Prototipe Vaporizer untuk Sistem LPG Conventer Kit Tipe Mixer untuk Mobil EFI dengan Kapasitas Maksimum 2000 cc', '2013', 2, '', 65000000),
(37, 26, 3, 'Optimasi Pengaktifan Motor Penggerak Pada Prototipe Sepeda Motor Hibrid untuk Menurunkan Konsumsi Bahan Bakar', '2013', 2, '', 42900000),
(38, 35, 3, 'Aplikasi Sistem Keuangan Musyarakah Mutanaqishah Perbankan Syariah sebagai Instrumen Pengembangan UMKM Di Indonesia', '2013', 2, '', 50000000),
(39, 27, 3, 'Pemodelan dan Penerapan Pengelolaan Sumber Daya Terpadu untuk Suatu "Enterprise" Perguruan Tinggi (Studi Kasus : Politeknik Negeri Bandung)', '2013', 2, '', 40000000),
(40, 27, 5, 'Framework Penelitian Nasional Berbasis Ontology (Ontological National Research Framework)', '2014', 2, '', 80000000);

DROP TABLE IF EXISTS `peran_peneliti`;
CREATE TABLE IF NOT EXISTS `peran_peneliti` (
  `ID_PENELITI` int(11) NOT NULL,
  `ID_PENELITIAN` int(11) NOT NULL,
  `POSISI` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID_PENELITI`,`ID_PENELITIAN`),
  KEY `FK_DITELITI` (`ID_PENELITIAN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `peran_peneliti` (`ID_PENELITI`, `ID_PENELITIAN`, `POSISI`) VALUES
(1, 1, 0),
(2, 2, 0),
(2, 30, 1),
(3, 3, 0),
(4, 4, 0),
(5, 5, 0),
(6, 6, 0),
(7, 7, 0),
(8, 8, 0),
(9, 9, 0),
(10, 10, 0),
(11, 11, 0),
(12, 12, 0),
(13, 13, 0),
(14, 14, 0),
(15, 15, 0),
(16, 16, 0),
(17, 17, 0),
(18, 18, 0),
(19, 19, 0),
(20, 20, 0),
(21, 21, 0),
(22, 22, 0),
(23, 23, 0),
(24, 24, 0),
(25, 25, 0),
(26, 26, 0),
(27, 27, 0),
(28, 27, 1),
(29, 27, 1),
(30, 27, 1),
(31, 28, 0),
(32, 28, 1),
(33, 28, 1),
(34, 28, 1),
(35, 29, 0),
(36, 29, 1),
(37, 29, 1),
(38, 29, 1),
(39, 30, 0),
(40, 30, 1),
(42, 31, 0),
(43, 31, 1),
(44, 31, 1),
(45, 32, 0),
(46, 32, 1),
(47, 32, 1),
(47, 35, 1),
(48, 33, 0),
(49, 33, 1),
(50, 33, 1),
(51, 34, 0),
(52, 34, 1),
(53, 34, 1),
(54, 35, 0),
(54, 40, 1),
(56, 36, 0),
(57, 36, 1),
(58, 37, 0),
(59, 37, 1),
(60, 38, 0),
(61, 38, 1),
(62, 39, 0),
(62, 40, 1),
(63, 39, 1),
(63, 40, 0),
(64, 40, 1);

DROP TABLE IF EXISTS `progress_penelitian`;
CREATE TABLE IF NOT EXISTS `progress_penelitian` (
  `ID_PROGRESS` int(11) NOT NULL,
  `ID_PENELITIAN` int(11) NOT NULL,
  `TGL_PELAPORAN` date NOT NULL,
  `PROGRESS_FISIK` decimal(5,2) DEFAULT NULL,
  `PROGRESS_DANA` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`ID_PROGRESS`,`ID_PENELITIAN`),
  KEY `FK_PROGRESS_PENELITIAN` (`ID_PENELITIAN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `ref_kelompok_bidang_kajian`;
CREATE TABLE IF NOT EXISTS `ref_kelompok_bidang_kajian` (
  `ID_BIDANG_KAJIAN` int(11) NOT NULL AUTO_INCREMENT,
  `BIDANG_KAJIAN` varchar(150) NOT NULL,
  `KETERANGAN` varchar(300) DEFAULT NULL,
  `KODE` varchar(10) DEFAULT NULL,
  `LEVEL` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_BIDANG_KAJIAN`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=90 ;

INSERT INTO `ref_kelompok_bidang_kajian` (`ID_BIDANG_KAJIAN`, `BIDANG_KAJIAN`, `KETERANGAN`, `KODE`, `LEVEL`) VALUES
(1, 'MATEMATIKA DAN ILMU PENGETAHUAN ALAM (MIPA)', '', '100', 1),
(2, 'ILMU IPA', '', '110', 2),
(3, 'MATEMATIKA', '', '120', 2),
(4, 'KEBUMIAN DAN ANGKASA', '', '130', 2),
(5, 'ILMU TANAMAN', '', '140', 1),
(6, 'ILMU PERTANIAN DAN PERKEBUNAN', '', '150', 2),
(7, 'TEKNOLOGI DALAM ILMU TANAMAN', '', '160', 2),
(8, 'ILMU SOSIOLOGI PERTANIAN', '', '180', 2),
(9, 'ILMU KEHUTANAN', '', '190', 2),
(10, 'ILMU HEWANI', '', '200', 1),
(11, 'ILMU PETERNAKAN', '', '210', 2),
(12, 'ILMU PERIKANAN', '', '230', 2),
(13, 'ILMU KEDOKTERAN HEWAN', '', '250', 2),
(14, 'ILMU KEDOKTERAN', '', '260', 1),
(15, 'ILMU KEDOKTERAN SPESIALIS', '', '270', 2),
(16, 'ILMU KEDOKTERAN (AKADEMIK)', '', '300', 2),
(17, 'ILMU SPESIALIS KEDOKTERAN GIGI DAN MULUT', '', '320', 2),
(18, 'ILMU KEDOKTERAN GIGI (AKADEMIK)', '', '330', 2),
(19, 'ILMU KESEHATAN', '', '340', 1),
(20, 'ILMU KESEHATAN UMUM', '', '350', 2),
(21, 'ILMU KEPERAWATAN DAN KEBIDANAN', '', '370', 2),
(22, 'ILMU PSIKOLOGI', '', '390', 2),
(23, 'ILMU FARMASI', '', '400', 2),
(24, 'ILMU TEKNIK', '', '410', 1),
(25, 'TEKNIK SIPIL DAN PERENCANAAN TATA RUANG', '', '420', 2),
(26, 'ILMU KETEKNIKAN INDUSTRI', '', '430', 2),
(27, 'TEKNIK ELEKTRO DAN INFORMATIKA', '', '450', 2),
(28, 'TEKNOLOGI KEBUMIAN', '', '470', 2),
(29, 'ILMU PERKAPALAN', '', '480', 2),
(30, 'ILMU BAHASA', '', '500', 1),
(31, 'SUB RMPUN ILMU SASTRA (DAN BAHASA) INDONESIA DAN DAERAH', '', '510', 2),
(32, 'ILMU BAHASA', '', '520', 2),
(33, 'ILMU BAHASA ASING', '', '530', 2),
(34, 'ILMU EKONOMI', '', '550', 1),
(35, 'ILMU EKONOMI', '', '560', 2),
(36, 'ILMU MANAJEMEN', '', '570', 2),
(37, 'ILMU SOSIAL HUMANIORA', '', '580', 1),
(38, 'ILMU POLITIK', '', '590', 2),
(39, 'ILMU SOSIAL', '', '610', 2),
(40, 'AGAMA DAN FILSAFAT', '', '630', 1),
(41, 'ILMU PENGETAHUAN (ILMU) AGAMA', '', '640', 2),
(42, 'ILMU FILSAFAT', '', '650', 2),
(43, 'ILMU SENI, DESAIN DAN MEDIA', '', '660', 1),
(44, 'ILMU SENI PERTUNJUKAN', '', '670', 2),
(45, 'ILMU KESENIAN', '', '680', 2),
(46, 'ILMU SENI KRIYA', '', '690', 2),
(47, 'ILMU MEDIA', '', '700', 2),
(48, 'DESAIN', '', '706', 2),
(49, 'ILMU PENDIDIKAN', '', '710', 1),
(50, 'PENDIDIKAN ILMU SOSIAL', '', '720', 2),
(51, 'ILMU PENDIDIKAN BAHASA DAN SASTRA', '', '740', 2),
(52, 'ILMU PENDIDIKAN OLAH RAGA DAN KESEHATAN', '', '760', 2),
(53, 'ILMU PENDIDIKAN MATEMATIKA DAN ILMU PENGETAHUAN ALAM (MIPA)', '', '770', 2),
(54, 'ILMU PENDIDIKAN TEKNOLOGI DAN KEJURUAN', '', '780', 2),
(55, 'ILMU PENDIDIKAN', '', '790', 2),
(56, 'ILMU PENDIDIKAN KESENIAN', '', '810', 2),
(57, 'RUMPUN ILMU LAINNYA', '', '900', 1),
(58, 'Teknologi Pangan', '', '10', 1),
(59, 'Riset Pengembangan  Pertanian Pangan', ' (padi, jagung, kedelai, lahan sub optimal)', '10.01', 2),
(60, 'Riset Pengembangan  Peternakan', ' (teknologi formulasi dan produksi pakan ternak)', '10.02', 2),
(61, 'Riset Pengembangan  Perkebunan ', '(benih unggul, budidaya dan rekayasa alsin, produk turunan kelapa sawit dan kakao)', '10.03', 2),
(62, 'Riset Perikanan Budidaya Berkelanjutan', ' (udang, bandeng, seabass, dll)', '10.04', 2),
(63, 'Riset Pengelolaan Perikanan Laut dengan pendekatan Ekosistem ', ' (tuna, udang, cakalang)', '10.05', 2),
(64, 'Teknologi Kesehatan dan Obat', '', '20', 1),
(65, 'Riset Pengembangan  Vaksin ', '(tuberkulosis, dengue, H5N1, hepatitis B)', '20.01', 2),
(66, 'Riset Pengembangan  Bahan Baku Obat  ', '(artemisin dan antibiotika)', '20.02', 2),
(67, 'Riset Pengembangan  Jamu', ' (anti hipertensi, anti hiper kolesterol, anti hiperurisemia)', '20.03', 2),
(68, 'Riset Pengembangan  Alat Kesehatan', '', '20.04', 2),
(69, 'Teknologi Energi', '', '30', 1),
(70, 'Riset Pengembangan  Pengolahan Batubara', '', '30.01', 2),
(71, 'Riset Pengembangan  Pembangkit Listrik dari Energi Baru dan Terbarukan', ' (Panas Bumi, Energi Surya, Energi Angin, Energi Laut, Bahan Bakar Nabati)', '30.02', 2),
(72, 'Teknologi Transportasi', '', '40', 1),
(73, 'Riset Pengembangan  Kendaraan Ramah Lingkungan dan Kendaraan Angkutan Umum Murah untuk Pedesaan', '', '40.01', 2),
(74, 'Riset Pengembangan  Perkapalan', '', '40.02', 2),
(75, 'Riset Pengembangan  Pesawat Komuter', '', '40.03', 2),
(76, 'Riset Pengembangan  Keselamatan Sistem Kereta Api', '', '40.04', 2),
(77, 'Teknologi Informasi dan Komunikasi', '', '50', 1),
(78, 'Riset Pengembangan  Telematika', '', '50.01', 2),
(79, 'Riset Pengembangan  Teknologi Content Industri Kreatif', '', '50.02', 2),
(80, 'Riset Pengembangan  Satelit Komunikasi', '', '50.03', 2),
(81, 'Riset Pengembangan  Teknologi Radar', '', '50.04', 2),
(82, 'Teknologi Pertahanan dan Keamanan', '', '60', 1),
(83, 'Riset Pengembangan  Rudal', '', '60.01', 2),
(84, 'Riset Pengembangan  Kapal Perang', '', '60.02', 2),
(85, 'Riset Pengembangan  Kendaran Tempur', '', '60.03', 2),
(86, 'Teknologi Material', '', '70', 1),
(87, 'Riset Pengembangan  Wafer Silikon Polikristal', '', '70.01', 2),
(88, 'Riset Pengembangan Teknologi untuk material khusus', ' (baja tahan peluru, magnet, carbon composite, dll.)', '70.02', 2),
(89, 'Riset Pengembangan  Teknologi pengolahan Sumberdaya Lokal menjadi bahan baku material substitusi impor ', '(bauksit, nikel, biji besi)', '70.03', 2);

DROP TABLE IF EXISTS `ref_luaran`;
CREATE TABLE IF NOT EXISTS `ref_luaran` (
  `ID_LUARAN` int(11) NOT NULL AUTO_INCREMENT,
  `LUARAN` varchar(100) NOT NULL,
  `DESKRIPSI` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ID_LUARAN`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

INSERT INTO `ref_luaran` (`ID_LUARAN`, `LUARAN`, `DESKRIPSI`) VALUES
(1, 'Publikasi Ilmiah', 'Tingkat Internasional'),
(2, 'Publikasi Ilmiah', 'Tingkat Nasional'),
(3, 'Publikasi Ilmiah', 'Tingkat Lokal'),
(4, 'Pemakalah dalam pertemuan Ilmiah', 'Tingkat Internasional'),
(5, 'Pemakalah dalam pertemuan Ilmiah', 'Tingkat Nasional'),
(6, 'Pemakalah dalam pertemuan Ilmiah', 'Tingkat Lokal'),
(7, 'Keynote Speaker dalam Pertemuan Ilmiah', 'Tingkat Internasional'),
(8, 'Keynote Speaker dalam Pertemuan Ilmiah', 'Tingkat Nasional'),
(9, 'Keynote Speaker dalam Pertemuan Ilmiah', 'Tingkat Lokal'),
(10, 'Visiting Lecturer', 'Tingkat Internasional'),
(11, 'Hak Atas Kekayaan Intelektual (KHI)', 'Paten'),
(12, 'Hak Atas Kekayaan Intelektual (KHI)', 'Paten Sederhana'),
(13, 'Hak Atas Kekayaan Intelektual (KHI)', 'Hak Cipta'),
(14, 'Hak Atas Kekayaan Intelektual (KHI)', 'Merek Dagang'),
(15, 'Hak Atas Kekayaan Intelektual (KHI)', 'Rahasia Dagang'),
(16, 'Hak Atas Kekayaan Intelektual (KHI)', 'Desain Produk Industri'),
(17, 'Hak Atas Kekayaan Intelektual (KHI)', 'Indikasi Geografis'),
(18, 'Hak Atas Kekayaan Intelektual (KHI)', 'Perlindungan Varietas Tanaman'),
(19, 'Hak Atas Kekayaan Intelektual (KHI)', 'Perlindungan Topografi Sirkuit Terpadu'),
(20, 'Teknologi Tepat Guna', ''),
(21, 'Model/Prototype/Desain/Karya Seni/Rekayasa Sosial', ''),
(22, 'Buku Ajar (ISBN)', ''),
(23, 'Laporan Penelitian yang tidak dipublikasikan', ''),
(24, 'Jumlah Dana Kerjasama Penelitian ', 'Tingkat Internasional'),
(25, 'Jumlah Dana Kerjasama Penelitian ', 'Tingkat Nasional'),
(26, 'Jumlah Dana Kerjasama Penelitian ', 'Tingkat Lokal');

DROP TABLE IF EXISTS `ref_metodologi`;
CREATE TABLE IF NOT EXISTS `ref_metodologi` (
  `ID_METODOLOGI` int(11) NOT NULL AUTO_INCREMENT,
  `METODOLOGI` varchar(250) NOT NULL,
  `KETERANGAN` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`ID_METODOLOGI`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

INSERT INTO `ref_metodologi` (`ID_METODOLOGI`, `METODOLOGI`, `KETERANGAN`) VALUES
(1, 'Analytical', 'Kuantitatif'),
(2, 'Case Study', 'Kualitatif'),
(3, 'Comparative', 'Kuantitatif'),
(4, 'Correlational-predictive', 'Kualitatif'),
(5, 'Design and demonstration', 'Kualitatif'),
(6, 'Evaluation', 'Kuantitatif'),
(7, 'Developmental', 'Kualitatif'),
(8, 'Experimental', 'Kuantitatif'),
(9, 'Exploratory', 'Kualitatif'),
(10, 'Historical', 'Kuantitatif'),
(11, 'Meta-analysis', 'Kuantitatif'),
(12, 'Methodological', 'Kuantitatif');

DROP TABLE IF EXISTS `ref_penyandang_dana`;
CREATE TABLE IF NOT EXISTS `ref_penyandang_dana` (
  `ID_PENYANDANG_DANA` int(11) NOT NULL AUTO_INCREMENT,
  `PENYANDANG_DANA` varchar(100) NOT NULL,
  `NAMA_KEL_PENYANDANG_DANA` varchar(100) NOT NULL,
  PRIMARY KEY (`ID_PENYANDANG_DANA`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

INSERT INTO `ref_penyandang_dana` (`ID_PENYANDANG_DANA`, `PENYANDANG_DANA`, `NAMA_KEL_PENYANDANG_DANA`) VALUES
(1, 'Penelitian Negeri', 'Menristek'),
(2, 'Penelitian Negeri', 'Dikti'),
(3, 'Penelitian Swasta', 'Toray'),
(4, 'Penelitian Swasta', 'Individu');

DROP TABLE IF EXISTS `ref_persyaratan`;
CREATE TABLE IF NOT EXISTS `ref_persyaratan` (
  `ID_SYARAT` int(11) NOT NULL AUTO_INCREMENT,
  `ID_SKEMA` int(11) NOT NULL,
  `SYARAT` varchar(250) NOT NULL,
  `DESKRIPSI` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ID_SYARAT`),
  KEY `FK_TERDIRI` (`ID_SKEMA`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

INSERT INTO `ref_persyaratan` (`ID_SYARAT`, `ID_SKEMA`, `SYARAT`, `DESKRIPSI`) VALUES
(1, 7, 'Ketua peneliti bergelar doktor atau S-2 dengan jabatan lektor kepala', ''),
(2, 7, 'Anggota peneliti minimum 1 dan maksimum 3 orang', ''),
(3, 7, 'Salah satu anggota peneliti harus bergelar doktor', ''),
(4, 8, 'Ketua peneliti bergelar doktor', ''),
(5, 8, 'Anggota peneliti minimum 1 dan maksimum 2orang', ''),
(6, 8, 'Salah satu anggota peneliti harus bergelar doktor', ''),
(7, 8, 'Menyertakan minimum 4 bimbingan mahasiswa S-2 atau 2 mahasiswa S-3, atau 2 mahasiswa S-2 dan 1 mahasiswa S-3', ''),
(8, 4, 'Ketua peneliti bergelar doktor atau S-2 dengan jabatan fungsional minimum lektor kepala dan boleh berstatus mahasiswa S-3', ''),
(9, 4, 'Anggota peneliti minimum 1 dan maksimum 2', ''),
(10, 3, 'Ketua peneliti minimum S-2, jabatan fungsional minimum lektor, dan boleh berstatus mahasiswa S-3', ''),
(11, 3, 'Anggota peneliti minimum 1 dan maksimum 2 orang', ''),
(12, 9, 'Ketua Tim Peneliti Pengusul (TPP) maksimum bergelar S-2', ''),
(13, 9, 'TPP mempunyai anggota peneliti minimum 1 dan maksimum 2 orang', ''),
(14, 9, 'Tim Peneliti Mitra (TPM) terdiri atas ketua dan 1 orang anggota, keduanya bergelar doktor', ''),
(15, 10, 'Satu (1) mahasiswa aktif program doktor', ''),
(16, 11, 'Ketua maksimum bergelar S-2 dengan jabatan fungsional maksimum lektor', ''),
(17, 11, 'Anggota peneliti minimum 1 dan maksimum 2 orang', ''),
(18, 11, 'Semua peneliti berasal dari perguruan tinggi kelompok binaan', ''),
(19, 12, 'Ketua Peneliti bergelar doktor', ''),
(20, 12, 'Anggota peneliti minimum 2 maksimum 5 orang', ''),
(21, 12, 'Salah satu anggota dari PT harus bergelar doktor', ''),
(22, 12, 'Maksimum 2 anggota peneliti berasal dari praktisi (mitra)', ''),
(23, 13, 'Ketua peneliti berasal dari PT bergelar doktor', ''),
(24, 13, 'Anggota peneliti dari PT minimum 1 dan maksimum 3 orang', ''),
(25, 13, 'Satu anggota peneliti dari PT harus bergelar doktor', ''),
(26, 13, 'Anggota peneliti dari mitra tidak wajib ada dan maksimum sebanyak 2 orang', ''),
(27, 14, 'Ketua peneliti bergelar doktor', ''),
(28, 14, 'Anggota peneliti minimum 1 dan maksimum 2 orang', ''),
(29, 14, 'Satu anggota peneliti dari PT harus bergelar doktor', ''),
(30, 15, 'Ketua peneliti bergelar doktor', ''),
(31, 15, 'Anggota peneliti maksimum berjumlah 2 orang', ''),
(32, 15, 'Salah satu anggota peneliti harus bergelar doktor', ''),
(33, 5, 'Ketua peneliti bergelar doktor', ''),
(34, 5, 'Anggota peneliti minimum 1 dan maksimum 2 orang', ''),
(35, 5, 'Salah satu anggota peneliti harus bergelar doktor', ''),
(36, 6, 'Ketua peneliti berasal dari PT bergelar doktor', ''),
(37, 6, 'Jumlah anggota peneliti minimum 1 dan maksimum 3 orang', ''),
(38, 6, 'Satu anggota peneliti dari PT harus bergelar doktor', '');

DROP TABLE IF EXISTS `ref_progress`;
CREATE TABLE IF NOT EXISTS `ref_progress` (
  `ID_PROGRESS` int(11) NOT NULL AUTO_INCREMENT,
  `TAHAP` varchar(250) NOT NULL,
  `KETERANGAN` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`ID_PROGRESS`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

INSERT INTO `ref_progress` (`ID_PROGRESS`, `TAHAP`, `KETERANGAN`) VALUES
(1, 'Pendaftaran pengusul penelitian', 'Persiapan/Pengusulan'),
(2, 'Pengisian Identitas Pengusul', 'Persiapan/Pengusulan'),
(3, 'Unggah pra proposal', 'Persiapan/Pengusulan'),
(4, 'Unggah proposal', 'Persiapan/Pengusulan'),
(5, 'Evaluasi pra proposal', 'Seleksi'),
(6, 'Evaluasi proposal', 'Seleksi'),
(7, 'Evaluasi pembahasan proposal', 'Seleksi'),
(8, 'Evaluasi site visit', 'Seleksi'),
(9, 'Penetapan grantee', 'Seleksi'),
(10, 'Catatan harian dan Laporan kemajuan 3 bulanan', 'Pelaksanaan penelitian'),
(11, 'Monev internal', 'Pelaksanaan penelitian'),
(12, 'Monev eksternal', 'Pelaksanaan penelitian'),
(13, 'Laporan akhir', 'Pelaksanaan penelitian'),
(14, 'Pembahasan hasil/ kelayakan', 'Tindak Lanjut');

DROP TABLE IF EXISTS `ref_skema_penelitian`;
CREATE TABLE IF NOT EXISTS `ref_skema_penelitian` (
  `ID_SKEMA` int(11) NOT NULL AUTO_INCREMENT,
  `ID_PENYANDANG_DANA` int(11) NOT NULL,
  `SKEMA_PENELITIAN` varchar(100) NOT NULL,
  `SUB_SKEMA` varchar(100) DEFAULT NULL,
  `DURASI_MIN` decimal(4,2) DEFAULT NULL,
  `DURASI_MAKS` decimal(4,2) DEFAULT NULL,
  `BIAYA_MIN` decimal(12,0) DEFAULT NULL,
  `BIAYA_MAKS` decimal(12,0) DEFAULT NULL,
  PRIMARY KEY (`ID_SKEMA`),
  KEY `FK_MENDANAI` (`ID_PENYANDANG_DANA`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

INSERT INTO `ref_skema_penelitian` (`ID_SKEMA`, `ID_PENYANDANG_DANA`, `SKEMA_PENELITIAN`, `SUB_SKEMA`, `DURASI_MIN`, `DURASI_MAKS`, `BIAYA_MIN`, `BIAYA_MAKS`) VALUES
(1, 1, 'Insinas', 'Riset Terapan', NULL, NULL, NULL, NULL),
(2, 1, 'Insinas', 'Riset Dasar', NULL, NULL, NULL, NULL),
(3, 2, 'Desentralisasi', 'Penelitian Hibah Bersaing', 2.00, 3.00, 50000000, 75000000),
(4, 2, 'Desentralisasi', 'Penelitian Fundamental ', 1.00, 2.00, 50000000, 75000000),
(5, 2, 'Kompetitif Nasional', 'Penelitian Strategis Nasional (Stranas)', 2.00, 3.00, 75000000, 100000000),
(6, 2, 'Kompetitif Nasional', 'Penelitian Prioritas Nasional-Master Plan Percepatan Pembangunan Ekonomi\r\nIndonesia (MP3EI)', 2.00, 3.00, 150000000, 200000000),
(7, 2, 'Desentralisasi', 'Penelitian Unggulan Perguruan Tinggi (PUPT)', 2.00, 5.00, 50000000, NULL),
(8, 2, 'Desentralisasi', 'Penelitian Tim Pascasarjana (PPS)', 3.00, NULL, 100000000, 125000000),
(9, 2, 'Desentralisasi', 'Penelitian Kerjasama Antar Perguruan Tinggi (PEKERTI)', 2.00, NULL, 75000000, 100000000),
(10, 2, 'Desentralisasi', 'Penelitian Disertasi Doktor (PDD)', 1.00, NULL, 30000000, 50000000),
(11, 2, 'Desentralisasi', 'Penelitian Dosen Pemula (PDP)', 1.00, NULL, 10000000, 15000000),
(12, 2, 'Kompetitif Nasional', 'Penelitian Unggulan Strategis Nasional (PUSNAS)', 2.00, 3.00, 500000000, 1000000000),
(13, 2, 'Kompetitif Nasional', 'Riset Andalan Perguruan Tinggi dan Industri (RAPID)', 2.00, 3.00, 300000000, 400000000),
(14, 2, 'Kompetitif Nasional', 'Penelitian Kerjasama Luar Negeri dan Publikasi Internasional (KLN)', 2.00, 3.00, 150000000, 200000000),
(15, 2, 'Kompetitif Nasional', 'Penelitian Kompetensi (HIKOM)', 2.00, 3.00, 100000000, 150000000),
(16, 1, 'Insinas', 'Riset peningkatan Kapasitas Iptek Sistem Produksi (KP) ', NULL, NULL, NULL, NULL),
(17, 1, 'Insinas', 'Percepatan Difusi dan Pemanfaatan Iptek (DF) ', NULL, NULL, NULL, NULL);

DROP TABLE IF EXISTS `resume_penelitian`;
CREATE TABLE IF NOT EXISTS `resume_penelitian` (
  `ID_ABSTRAKSI` int(11) NOT NULL AUTO_INCREMENT,
  `ID_PENELITIAN` int(11) NOT NULL,
  `ABSTRAKSI` varchar(5000) NOT NULL,
  `KEYWORD` varchar(500) DEFAULT NULL,
  `KETERANGAN` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`ID_ABSTRAKSI`),
  KEY `FK_MEMILIKIABSTRAKSI` (`ID_PENELITIAN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


ALTER TABLE `luaran_penelitian`
  ADD CONSTRAINT `FK_LUARAN_PENELITIAN` FOREIGN KEY (`ID_LUARAN`) REFERENCES `ref_luaran` (`ID_LUARAN`),
  ADD CONSTRAINT `FK_LUARAN_PENELITIAN2` FOREIGN KEY (`ID_PENELITIAN`) REFERENCES `penelitian` (`ID_PENELITIAN`);

ALTER TABLE `master_peneliti`
  ADD CONSTRAINT `FK_BERASAL_DARI` FOREIGN KEY (`ID_INSTITUSI`) REFERENCES `master_institusi_peneliti` (`ID_INSTITUSI`);

ALTER TABLE `metodologi_penelitian`
  ADD CONSTRAINT `FK_METODOLOGI_PENELITIAN` FOREIGN KEY (`ID_PENELITIAN`) REFERENCES `penelitian` (`ID_PENELITIAN`),
  ADD CONSTRAINT `FK_METODOLOGI_PENELITIAN2` FOREIGN KEY (`ID_METODOLOGI`) REFERENCES `ref_metodologi` (`ID_METODOLOGI`);

ALTER TABLE `penelitian`
  ADD CONSTRAINT `FK_BIDANG_KAJIANPENELITIAN` FOREIGN KEY (`ID_BIDANG_KAJIAN`) REFERENCES `ref_kelompok_bidang_kajian` (`ID_BIDANG_KAJIAN`),
  ADD CONSTRAINT `FK_MEMILIKI` FOREIGN KEY (`ID_SKEMA`) REFERENCES `ref_skema_penelitian` (`ID_SKEMA`);

ALTER TABLE `peran_peneliti`
  ADD CONSTRAINT `FK_DITELITI` FOREIGN KEY (`ID_PENELITIAN`) REFERENCES `penelitian` (`ID_PENELITIAN`),
  ADD CONSTRAINT `FK_MENELITI` FOREIGN KEY (`ID_PENELITI`) REFERENCES `master_peneliti` (`ID_PENELITI`);

ALTER TABLE `progress_penelitian`
  ADD CONSTRAINT `FK_PROGRESS` FOREIGN KEY (`ID_PROGRESS`) REFERENCES `ref_progress` (`ID_PROGRESS`),
  ADD CONSTRAINT `FK_PROGRESS_PENELITIAN` FOREIGN KEY (`ID_PENELITIAN`) REFERENCES `penelitian` (`ID_PENELITIAN`);

ALTER TABLE `ref_persyaratan`
  ADD CONSTRAINT `FK_TERDIRI` FOREIGN KEY (`ID_SKEMA`) REFERENCES `ref_skema_penelitian` (`ID_SKEMA`);

ALTER TABLE `ref_skema_penelitian`
  ADD CONSTRAINT `FK_MENDANAI` FOREIGN KEY (`ID_PENYANDANG_DANA`) REFERENCES `ref_penyandang_dana` (`ID_PENYANDANG_DANA`);

ALTER TABLE `resume_penelitian`
  ADD CONSTRAINT `FK_MEMILIKIABSTRAKSI` FOREIGN KEY (`ID_PENELITIAN`) REFERENCES `penelitian` (`ID_PENELITIAN`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
