package com.sip.web;
import com.sip.domain.ProgressPenelitian;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import com.sip.domain.ProgressPenelitianBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/progresspenelitians")
@Controller
@RooWebScaffold(path = "progresspenelitians", formBackingObject = ProgressPenelitian.class)
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = ProgressPenelitianBatchService.class)
@GvNIXDatatables(ajax = true)
public class ProgressPenelitianController {
}
