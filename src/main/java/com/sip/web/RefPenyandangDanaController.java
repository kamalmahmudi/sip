package com.sip.web;
import com.sip.domain.RefPenyandangDana;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import com.sip.domain.RefPenyandangDanaBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/refpenyandangdanas")
@Controller
@RooWebScaffold(path = "refpenyandangdanas", formBackingObject = RefPenyandangDana.class)
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = RefPenyandangDanaBatchService.class)
@GvNIXDatatables(ajax = true)
public class RefPenyandangDanaController {
}
