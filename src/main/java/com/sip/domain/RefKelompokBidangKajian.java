package com.sip.domain;
import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooJpaActiveRecord(versionField = "", table = "ref_kelompok_bidang_kajian")
@RooDbManaged(automaticallyDelete = true)
@RooToString(excludeFields = { "penelitians" })
public class RefKelompokBidangKajian {
}
