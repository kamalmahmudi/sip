package com.sip.web;
import com.sip.domain.MasterInstitusiPeneliti;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import com.sip.domain.MasterInstitusiPenelitiBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/masterinstitusipenelitis")
@Controller
@RooWebScaffold(path = "masterinstitusipenelitis", formBackingObject = MasterInstitusiPeneliti.class)
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = MasterInstitusiPenelitiBatchService.class)
@GvNIXDatatables(ajax = true)
public class MasterInstitusiPenelitiController {
}
