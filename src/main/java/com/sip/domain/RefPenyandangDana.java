package com.sip.domain;
import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooJpaActiveRecord(versionField = "", table = "ref_penyandang_dana")
@RooDbManaged(automaticallyDelete = true)
@RooToString(excludeFields = { "refSkemaPenelitians" })
public class RefPenyandangDana {
}
