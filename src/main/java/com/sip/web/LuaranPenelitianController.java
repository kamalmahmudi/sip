package com.sip.web;
import com.sip.domain.LuaranPenelitian;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import com.sip.domain.LuaranPenelitianBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/luaranpenelitians")
@Controller
@RooWebScaffold(path = "luaranpenelitians", formBackingObject = LuaranPenelitian.class)
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = LuaranPenelitianBatchService.class)
@GvNIXDatatables(ajax = true)
public class LuaranPenelitianController {
}
