package com.sip.web;
import com.sip.domain.RefLuaran;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.gvnix.addon.web.mvc.annotations.jquery.GvNIXWebJQuery;
import com.sip.domain.RefLuaranBatchService;
import org.gvnix.addon.web.mvc.annotations.batch.GvNIXWebJpaBatch;
import org.gvnix.addon.datatables.annotations.GvNIXDatatables;

@RequestMapping("/refluarans")
@Controller
@RooWebScaffold(path = "refluarans", formBackingObject = RefLuaran.class)
@GvNIXWebJQuery
@GvNIXWebJpaBatch(service = RefLuaranBatchService.class)
@GvNIXDatatables(ajax = true)
public class RefLuaranController {
}
