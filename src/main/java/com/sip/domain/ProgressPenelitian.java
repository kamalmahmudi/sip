package com.sip.domain;
import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooJpaActiveRecord(identifierType = ProgressPenelitianPK.class, versionField = "", table = "progress_penelitian")
@RooDbManaged(automaticallyDelete = true)
@RooToString(excludeFields = { "idProgress", "idPenelitian" })
public class ProgressPenelitian {
}
